# Pupscan : format du json dans le QRCode.

MAJ 4 juin 2019
Fonctionne avec la version 1.4.3

## Cas général

```` json
{
   "state":"CONNECTOR",
   "connector-field1":"xxxx",
   "connector-field2":"xxxx",
   ...
   "connector-fieldN":"xxxx",
   "documentType":"pdf",
   "documentQuality":"standard",
   "documentRender":"gray", 
   "cropOn":true,
   "flashOn":true,
   "wbgOn":true,
   "patternFilename":"",
   "name":"NOM DU SCENARIO",
   "color":"#ffffff",
   "timeZone":"Europe/Paris"
}
````

Les valeurs possibles pour les champs les plus utiles sont :
 - **cropOn**, activer/désactiver le crop (inactif pour l'instant): ```[ true | false ]```
 - **flashOn**, activer/désactiver le flash: ```[ true | false ]```
 - **wbgOn**, activer/désactiver le filtre white background: ```[ true | false ]```
 - **patternFilename**, customiser le nom du fichier: "" pour utiliser le format par défaut
 - **documentType**, format du fichier: ```["pdf" | "jpg" ]```
 - **documentQuality**, nombre de DPI:```["photo" | "standard" | "web"  ]``` (équivalent à full / 300DPI / 150 DPI)  
 - **documentRender**, couleur:```["color" | "gray" ]```  
 - **name**, nom du scenario
 - **color**, couleur du scenario en RGB hexadecimal


## Mailjet

```` json
{   
   "state":"mail-jet",
   "from":"my-pup@pupscan.com",
   "to":"xxxxxx@pupscan.com",
   "subject":"Ma facture",
   "documentType":"pdf",
   "documentQuality":"standard",
   "documentRender":"gray", 
   "cropOn":true,
   "flashOn":true,
   "wbgOn":true,
   "patternFilename":"",
   "name":"NOM DU SCENARIO",
   "color":"#ffffff",
   "timeZone":"Europe/Paris"
}
````

## FTP

```` json
{  
   "state":"ftp",
   "serverAddress":"myurl.com",
   "serverPort":"21",
   "login":"login",
   "password":"password",
   "repertoire":"directory",
   "documentType":"pdf",
   "documentQuality":"standard",
   "documentRender":"gray", 
   "cropOn":true,
   "flashOn":true,
   "wbgOn":true,
   "patternFilename":"",
   "name":"NOM DU SCENARIO",
   "color":"#ffffff",
   "timeZone":"Europe/Paris"
}
````




## Outils

Décoder une image QRCode
https://webqr.com/

Encoder un QRCode
https://zxing.appspot.com/generator/

Formater un JSON
https://jsonformatter.curiousconcept.com/

